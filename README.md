# read_rc
A simple rc (resource) file reader. Basically read in a line, parse it into a parameter string and a value string.

No real checks on boundaries have been done so use with caution.