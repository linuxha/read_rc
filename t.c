#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define _XOPEN_SOURCE /* glibc2 needs this */
#include <time.h>

int
main(int argc, char **argv) {
  char answer[100], *p;
  char *ptr;

  struct tm tm;
  char buf[255];
  char str[] = "800621";

  char variable[255];
  char value[255];

  // =========================================================================

  // Handle different times
  strptime("2001-11-12 18:31:01", "%Y-%m-%d %H:%M:%S", &tm);
  strftime(buf, sizeof(buf), "%d %b %Y %H:%M", &tm);
  puts(buf);			// 12 Nov 2001 18:31

  strptime(str, "%y%m%d", &tm);	// %02y%02m%02d doesn't work
  strftime(buf, sizeof(buf), "%d %b %Y %H:%M", &tm);
  puts(buf);			// 21 Jun 1980 18:31

  // =========================================================================

  // Handle reading in a config file
  char str2[] = "A = B\n";
  char str3[] = "#A = B\n";
  char str4[] = "A= B\n";
  char str5[] = "A\t= B\n";

  sscanf(str2, "%[^#=]=%s", &variable, &value );
  printf("Result: (%s) = (%s)\n", variable, value);

  strcpy(variable, "");
  strcpy(value, "");

  sscanf(str3, "%[^#=]=%s", &variable, &value );
  printf("Result: (%s) = (%s)\n", variable, value);

  sscanf(str4, "%[^#=]=%s", &variable, &value );
  printf("Result: (%s) = (%s)\n", variable, value);

  sscanf(str5, "%[^#=]=%s", &variable, &value );
  printf("Result: (%s) = (%s)\n", variable, value);

  /*
  ** Result: (A ) = (B)
  ** Result: () = ()
  ** Result: (A) = (B)
  ** Result: (A      ) = (B) // That's a tab after the A
  */

  // =========================================================================

  // Handle reading a line safely
  printf("Type something:\n");

  if(fgets(answer, sizeof(answer), stdin) == NULL) {
    printf("EOF\n");
  }

  printf("You typed \"%s\"\n", answer);

  if((p = strchr(answer, '\n')) != NULL) {
    *p = '\0';
  }

  printf("You typed \"%s\"\n", answer);

  exit(0);
}

